import os
import sys
import imp
import glob
import itertools
from argparse import ArgumentParser
from collections import OrderedDict

import numpy as np
import pyrap.tables as pt
from scipy.spatial import KDTree

import astropy.units as u
import astropy.constants as const
import astropy.time as at

import pyuvdata.utils as uvutils

sys.path.append('./ps_eor')

from ps_eor import datacube, psutil


sday_to_day = u.sday.to(u.day)

# HERA telescope location
telescope_location_lat_lon_alt = [-0.5361917820434705, 0.37399445489646055, 1051.6900000087917]

parser = ArgumentParser(description="Split input MS into several LST bins")
parser.add_argument('--config', '-c', help='Configuration file', required=False, default='config.py')


def get_config(filename):
    return imp.load_source('config', filename)


def baseline_to_antnums(baseline):
    if np.min(baseline) > 2**16:
        ant2 = (baseline - 2**16) % 2048 - 1
        ant1 = (baseline - 2**16 - (ant2 + 1)) / 2048 - 1
    else:
        ant2 = (baseline) % 256 - 1
        ant1 = (baseline - (ant2 + 1)) / 256 - 1
    return np.int32(ant1), np.int32(ant2)


def antnums_to_baseline(ant1, ant2):
    return np.int64(2048 * (ant2 + 1) + (ant1 + 1) + 2**16)


def get_baselines_config_from_uvd(uvd):
    antennae = {}
    lat, lon, alt = uvd.telescope_location_lat_lon_alt
    for i, antnum in enumerate(uvd.antenna_numbers):
        pos = uvd.antenna_positions[i, :] + uvd.telescope_location
        xyz = uvutils.ENU_from_ECEF(pos, latitude=lat, longitude=lon, altitude=alt)
        antennae[antnum] = {'x': xyz[0], 'y': xyz[1], 'z': xyz[2]}

    baselines = {}
    for b in np.unique(uvd.baseline_array):
        a1, a2 = uvd.baseline_to_antnums(b)
        dx = antennae[a2]['x'] - antennae[a1]['x']
        dy = antennae[a2]['y'] - antennae[a1]['y']
        baselines[b] = (dx, dy)

    return antennae, baselines


def get_baselines_config_from_ms(ms_table, use_enu=True, double_rotate=False):
    t_ant = pt.table(ms_table.getkeyword('ANTENNA'), readonly=True, ack=False)

    full_pos = np.array(t_ant.getcol('POSITION'))
    ant_nums = np.array(t_ant.getcol('NAME'))

    t_ant.close()

    idx_flagged = ant_nums == ''
    full_pos = full_pos[~idx_flagged]
    ant_nums = ant_nums[~idx_flagged]
    ant_nums = ant_nums.astype(int) - 1

    tel_loc = np.mean(full_pos, axis=0)
    rel_pos = full_pos - tel_loc

    if use_enu:
        lat, lon, alt = telescope_location_lat_lon_alt
        if double_rotate:
            rot = 2 * lon
        else:
            rot = lon
        rot_full_pos = uvutils.ECEF_from_rotECEF(rel_pos, rot)
        xyz = uvutils.ENU_from_ECEF((rot_full_pos + tel_loc).T, latitude=lat, longitude=lon, altitude=alt).T
    else:
        xyz = rel_pos

    antennae_ms = dict([(i, {'x': k[0], 'y': k[1], 'z': k[2]}) for i, k in zip(ant_nums, xyz)])

    baselines_ms = {}
    for a1, a2 in itertools.combinations(ant_nums, 2):
        b = antnums_to_baseline(a1, a2)
        dx = antennae_ms[a2]['x'] - antennae_ms[a1]['x']
        dy = antennae_ms[a2]['y'] - antennae_ms[a1]['y']
        baselines_ms[b] = (dx, dy)

    return antennae_ms, baselines_ms


def get_baselines_cluster(baselines):
    ''' Cluster the baselines using a KDTree '''
    max_dist = 0.5

    clusters = {(0, 0): []}
    kdtree = KDTree(clusters.keys())
    for b_id, baseline in baselines.items():
        res = kdtree.query(baseline, distance_upper_bound=max_dist)[1]
        if res < len(kdtree.data):
            clusters[tuple(kdtree.data[res])].append(b_id)
        else:
            clusters[tuple(baseline)] = [b_id]
            kdtree = KDTree(clusters.keys())

    clusters = OrderedDict(sorted(clusters.items(), key=lambda i: np.linalg.norm(i[0])))

    return clusters


def get_ms_freqs(ms_table):
    t_spec_win = pt.table(ms_table.getkeyword('SPECTRAL_WINDOW'), readonly=True, ack=False)
    freqs = t_spec_win.getcol('CHAN_FREQ').squeeze()
    t_spec_win.close()

    return freqs


def ms_time_to_lst(time, longitude):
    return at.Time(time / 3600. / 24., scale='utc',
                   format='mjd').sidereal_time('mean', longitude=longitude)


def get_lst_bin(ms_files, lst_res, longitude):
    lst_starts = []
    lst_ends = []
    for filename in ms_files:
        t = pt.table(filename, readonly=True, ack=False)
        lst_time = ms_time_to_lst(t.getcol('TIME'), longitude)
        t.close()
        lst_starts.append(lst_time.hour.min())
        lst_ends.append(lst_time.hour.max())

    return np.arange(np.min(lst_starts), np.max(lst_ends) + lst_res, lst_res)


def get_config_bool(config, param):
    if hasattr(config, param):
        return getattr(config, param)
    return False


def get_vis_mask(vis, mask):
    vis = np.ma.masked_array(vis, mask=mask)
    d = dict()
    if vis.shape[-1] != 4:
        d['i'] = vis[:, :, 0]
    else:
        d['xx'] = vis[:, :, 0]
        d['xy'] = vis[:, :, 1]
        d['yx'] = vis[:, :, 2]
        d['yy'] = vis[:, :, 3]
        d['I'] = 0.5 * (vis[:, :, 0] + vis[:, :, 3])
        d['Q'] = 0.5 * (vis[:, :, 0] - vis[:, :, 3])
        d['U'] = 0.5 * (vis[:, :, 1] + vis[:, :, 2])
        d['V'] = -0.5j * (vis[:, :, 1] - vis[:, :, 2])

    return d


def main():
    args = parser.parse_args(sys.argv[1:])

    config = get_config(args.config)
    ms_files = glob.glob(config.inp_ms_dir + '*.MS')

    if not os.path.exists(config.lst_dir):
        os.makedirs(config.lst_dir)

    lst_res = config.bin_res_min / 60. / sday_to_day
    lst_bins = get_lst_bin(ms_files, lst_res, config.longitude)
    print '\nLST binning from %.3f h to %.3f h with %.2f min resolution (%s bins)' % (lst_bins[0], lst_bins[-1],
                                                                                      config.bin_res_min,
                                                                                      len(lst_bins) - 1)

    for ms_filename in ms_files:
        print '\nProcessing:', os.path.basename(ms_filename)
        t = pt.table(ms_filename, readonly=True, ack=False)

        vis = t.getcol(config.data_col)
        flg = t.getcol('FLAG')

        if vis.shape[-1] != 4:
            print '- Not full stokes dataset'
            config.stokes_output = ['i']
        else:
            print '- Full stokes dataset'

        print '- Reading array configuration...'
        antennae, baselines = get_baselines_config_from_ms(t, get_config_bool(config, 'use_enu'),
                                                           get_config_bool(config, 'double_rotate'))

        print '- Clustering baselines...'
        clusters = get_baselines_cluster(baselines)
        print '  -> Found %s unique baseline/slope' % len(clusters.keys())
        # baselines_length = [np.sqrt(dx ** 2 + dy ** 2) for dx, dy in clusters.keys()]
        # print '  -> Uniques baseline length:', np.unique(baselines_length)

        time = t.getcol('TIME')
        lst_time = ms_time_to_lst(time, config.longitude)

        # Compute the bin array
        if config.do_lst_bin:
            lst_bin_ids = np.digitize(lst_time, lst_bins)
        else:
            lst_bin_ids = np.digitize(time, np.unique(time))

        # Compute the baseline ids
        ant1 = t.getcol('ANTENNA1')
        ant2 = t.getcol('ANTENNA2')
        b_ids = antnums_to_baseline(ant1, ant2)

        int_time = t.getcol("INTERVAL")[0]

        freqs = get_ms_freqs(t)
        lamb = const.c.value / (freqs)
        jy2k = ((1e-26 * lamb ** 2) / (2 * const.k_B.value))

        if config.split_even_odd:
            odd_times = np.in1d(time, np.unique(time)[1::2])
            even_times = np.in1d(time, np.unique(time)[::2])
            lst_bin_ids[odd_times] = 10000 + lst_bin_ids[odd_times]
            lst_bin_ids[even_times] = 20000 + lst_bin_ids[even_times]

        for lst_bin_id in np.unique(lst_bin_ids):
            # Get the data for this lst_bin
            c_vis = vis[lst_bin_ids == lst_bin_id]
            c_flags = flg[lst_bin_ids == lst_bin_id]
            c_b_ids = b_ids[lst_bin_ids == lst_bin_id]
            c_time = time[lst_bin_ids == lst_bin_id]
            c_lst_time = lst_time[lst_bin_ids == lst_bin_id].hour

            c_n_times = len(c_vis) / len(baselines)

            print '- %d: %.2f h -> %.2f h, %i visbilities' % (lst_bin_id, c_lst_time[0], c_lst_time[-1], len(c_vis))

            jd_start = at.Time(c_time[0] / 3600. / 24., scale='utc', format='mjd').jd

            vis_mask = get_vis_mask(c_vis, c_flags)

            # Looping over stokes parameters
            for stokes in config.stokes_output:
                assert stokes in vis_mask.keys()

                data = []
                weights = []
                uu = []
                vv = []

                # Average the data with same baseline/slope
                for (dx, dy), ids in clusters.items():
                    if (dx, dy) == (0, 0):
                        # skip autocorrelation
                        continue

                    if config.do_red_bas_averaging:
                        ind = np.in1d(c_b_ids, ids)
                        if np.sum(ind) == 0:
                            continue
                        vis_stokes_mask = vis_mask[stokes][ind]
                        data.append(np.ma.mean(vis_stokes_mask, axis=0) * jy2k)
                        weights.append(vis_stokes_mask.count(axis=0))
                        uu.append(dx)
                        vv.append(dy)
                    else:
                        for bid in ids:
                            ind = c_b_ids == bid
                            if np.sum(ind) == 0:
                                continue
                            vis_stokes_mask = vis_mask[stokes][ind]
                            if np.sum(~vis_stokes_mask.mask) == 0:
                                continue
                            data.append(np.ma.mean(vis_stokes_mask, axis=0) * jy2k)
                            weights.append(vis_stokes_mask.count(axis=0))
                            uu.append(dx)
                            vv.append(dy)

                # Save to h5 file
                meta = datacube.ImageMetaData.from_res(0.01, (100, 100))
                meta.set('PEINTTIM', int_time)
                meta.set('PETOTTIM', c_n_times * int_time)
                meta.set('PELSTSTA', c_lst_time[0])
                meta.set('PELSTEND', c_lst_time[-1])
                meta.set('PEJDSTAR', jd_start)

                uu = np.array(uu)
                vv = np.array(vv)

                w_cube = datacube.CartWeightsCubeMeter(np.array(weights, dtype=float).T, uu, vv, freqs, meta)
                cube = datacube.CartDataCubeMeter(np.array(data).T, uu, vv, freqs, meta, weights=w_cube)

                # Filter baselines
                cube.filter_uvrange(config.bmin, config.bmax)

                # Filter outliers
                outliers_a = cube.weights.data.sum(axis=1) == 0
                print '  -> Full flagged channels: %s' % ", ".join(['%.2f' % k for k in
                                                                    (cube.freqs[outliers_a] * 1e-6)])
                cube.filter_outliers(outliers_a)

                outliers_w = psutil.get_weights_outliers(cube.freqs, cube.weights.data, config.w_ratio, 0)
                print '  -> W outliers: %s' % ", ".join(['%.2f' % k for k in (cube.freqs[outliers_w] * 1e-6)])
                cube.filter_outliers(outliers_w)

                outliers_s = psutil.get_sefd_outliers(cube.freqs, cube.estimate_freqs_sefd(), config.sefd_ratio,
                                                      poly_fit_deg=3)
                print '  -> SEFD outliers: %s' % ", ".join(['%.2f' % k for k in (cube.freqs[outliers_s] * 1e-6)])
                cube.filter_outliers(outliers_s)

                output_fileame = config.lst_dir + \
                    '%s_cube_JD%s_RES%.1f_LST%s.h5' % (stokes, jd_start, config.bin_res_min, lst_bin_id)
                print '  -> saving to', output_fileame

                cube.save(output_fileame)

        t.close()


if __name__ == '__main__':
    main()
