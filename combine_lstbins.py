import os
import re
import imp
import sys
import glob
from argparse import ArgumentParser

sys.path.append('./ps_eor')

from ps_eor import datacube, psutil

parser = ArgumentParser(description="Combine input LST splitted cubes into LST combined cubes")
parser.add_argument('--config', '-c', help='Configuration file', required=False, default='config.py')


def get_config(filename):
    return imp.load_source('config', filename)


def main():
    args = parser.parse_args(sys.argv[1:])

    config = get_config(args.config)

    if not os.path.exists(config.lst_combined_dir):
        os.makedirs(config.lst_combined_dir)

    groups = {}
    print 'Getting LST bins...'
    for file in glob.glob(config.lst_dir + '*.h5'):
        filename = os.path.basename(file)
        match = re.match('i_cube_JD([\d\.]+)_RES([\d\.]+)_LST(\d+).h5', filename)
        if match is not None:
            jd, res, lst = match.groups()
            if (res, lst) not in groups:
                groups[(res, lst)] = [(jd, file)]
            else:
                groups[(res, lst)].append((jd, file))

    for (res, lst), jd_files in groups.items():
        print 'Combining LST bin number %s' % lst
        combiner = datacube.DataCubeCombiner(0, 300, weighting_mode='full')
        for jd, file in jd_files:
            cube = datacube.CartDataCubeMeter.load(file)
            cube.filter_uvrange(config.bmin, config.bmax)
            cube.filter_outliers(cube.weights.data.sum(axis=1) == 0)

            outliers = psutil.get_weights_outliers(cube.freqs, cube.weights.data, config.w_ratio, 0)
            cube.filter_outliers(outliers)

            outliers = psutil.get_sefd_outliers(cube.freqs, cube.estimate_freqs_sefd(), config.sefd_ratio,
                                                poly_fit_deg=3)
            cube.filter_outliers(outliers)

            combiner.add(cube, jd)

        combined_cube = combiner.get()

        print '-> combined nights:', combined_cube.meta.get('NIGHTS')

        output_fileame = config.lst_combined_dir + 'i_cube_RES%s_LST%s.h5' % (res, lst)
        print '-> saving to', output_fileame
        combined_cube.save(output_fileame)


if __name__ == '__main__':
    main()
