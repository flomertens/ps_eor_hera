import os
import imp
import sys
from argparse import ArgumentParser

sys.path.append('./ps_eor')

from ps_eor import datacube, fgfit, fitutil, pspec, psutil

parser = ArgumentParser(description="Run GPR on all input cubes")
parser.add_argument('--config', '-c', help='Configuration file', required=False, default='config.py')
parser.add_argument('--eor_bin', '-e', help='The EoR redshift bin', required=True, type=str)
parser.add_argument('--name', '-n', help='Add name to the filename/directory', required=False,
                    type=str, default='')
parser.add_argument('h5_files', help='Input data in h5 format', nargs='+')


def get_config(filename):
    return imp.load_source('config', filename)


def main():
    args = parser.parse_args(sys.argv[1:])

    config = get_config(args.config)
    gpr_config = fitutil.GprConfig.load(config.gpr_config_file)
    fitter = fgfit.GprForegroundFit(gpr_config)

    eor_bin_list = pspec.EorBinList.load(config.eor_bin_list)

    for filename in args.h5_files:
        print 'Running GPR on:', filename

        i_cube_meter = datacube.CartDataCubeMeter.load(filename)
        eor = eor_bin_list.get(args.eor_bin, freqs=i_cube_meter.freqs)

        i_cube_meter.filter_uvrange(config.bmin, config.bmax)
        i_cube = i_cube_meter.get_cube(eor.mfreq)

        i_cube.filter_outliers(i_cube.weights.data.sum(axis=1) == 0)

        outliers_w = psutil.get_weights_outliers(i_cube.freqs, i_cube.weights.data, config.w_ratio, 0)
        print '  -> W outliers: %s' % ", ".join(['%.2f' % k for k in (i_cube.freqs[outliers_w] * 1e-6)])
        i_cube.filter_outliers(outliers_w)

        outliers_s = psutil.get_sefd_outliers(i_cube.freqs, i_cube.estimate_freqs_sefd(), config.sefd_ratio,
                                              poly_fit_deg=3)
        print '  -> SEFD outliers: %s' % ", ".join(['%.2f' % k for k in (i_cube.freqs[outliers_s] * 1e-6)])
        i_cube.filter_outliers(outliers_s)

        noise_i = i_cube.make_diff_cube()
        fit_res = fitter.run(eor.get_slice_fg(i_cube), eor.get_slice_fg(noise_i))

        if args.name == '':
            prefix = 'gpr'
        else:
            prefix = 'gpr_%s' % args.name

        name = '%s_eor%s' % (prefix, eor.name + os.path.basename(filename).replace('i_cube', '').replace('.h5', ''))
        path = os.path.join(os.path.dirname(filename), name)

        fit_res.save(path, name)


if __name__ == '__main__':
    main()
