import os
import sys
import argparse

parser = argparse.ArgumentParser(description='Convert uvfits to MS using CASA')

parser.add_argument('files', help='Input uvfits files', nargs='+')
parser.add_argument('-o', '--out_dir', default='.', required=False, help='Output directory')


def main():
    args = parser.parse_args(sys.argv[3:])
    for fitsfile in args.files:
        vis = os.path.join(args.out_dir, os.path.basename(fitsfile).split('.')[0] + '.MS')

        if not os.path.exists(args.out_dir):
            os.makedirs(args.out_dir)

        if os.path.exists(vis):
            print 'Skipping %s, already existing' % vis
            continue

        print fitsfile, '->', vis

        importuvfits(fitsfile=fitsfile, vis=vis)

    print 'All done'


if __name__ == '__main__':
    main()
