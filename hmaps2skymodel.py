#!/usr/bin/env python

import os
import sys
from argparse import ArgumentParser

import numpy as np
import healpy as hp
from scipy import interpolate

import astropy.constants as const
import astropy.units as u

from astroutils import catalog

parser = ArgumentParser(description="Convert a set of healpix maps to astroutils SkyModel")
parser.add_argument('files', help='Input healpix maps in fits format', nargs='+')
parser.add_argument('--fs', help='Starting frequency in MHz', required=True, type=float)
parser.add_argument('--fe', help='Ending frequency in MHz', required=True, type=float)
parser.add_argument('--df', help='Ending frequency in MHz', required=True, type=float)
parser.add_argument('--out_dir', help='Output directory', default='.', type=str)
parser.add_argument('--out_name', help='Output directory', required=True, type=str)


f21 = 1.4204057 * 1e9 * u.Hz


def get_dec_ra(nside):
    theta, phi = hp.pixelfunc.pix2ang(nside, np.arange(hp.nside2npix(nside)))
    return theta, phi, - np.degrees(theta - np.pi / 2.), np.degrees(phi)


def z_to_freq(z):
    return f21.value / (z + 1)


def main():
    args = parser.parse_args(sys.argv[1:])
    hp_files = np.array(args.files)

    print 'Loading %s files ...' % len(hp_files)

    zs = np.array([float(f.split('_')[-1][1:-5]) for f in hp_files])

    hp_files = hp_files[np.argsort(zs)]
    zs = zs[np.argsort(zs)]
    freqs = z_to_freq(zs)

    print 'Input redshift: z~%.2f -> z~%.2f with dz~%.3f' % (zs.min(), zs.max(), np.diff(zs).mean())
    print 'Input frequency: f~%.2f MHZ-> z~%.2f MHz with df~%.3f MHz' % (freqs.min() * 1e-6, freqs.max() * 1e-6,
                                                                         abs(np.diff(freqs).mean()) * 1e-6)

    hp_maps = np.array([hp.read_map(f) for f in hp_files]).T

    nside = hp.npix2nside(hp_maps.shape[0])
    theta, phi, dec, ra = get_dec_ra(nside)

    # Convert kelvin maps to Jy
    hp_maps = hp_maps * (2 * const.k_B * freqs ** 2 / (const.c ** 2 * 1e-26)).value * hp.nside2pixarea(nside)

    # Interpolate in frequency
    output_freqs = np.arange(args.fs, args.fe + args.df, args.df) * 1e6

    print 'Interpolating to f~%.2f MHZ-> z~%.2f MHz with df~%.3f MHz ...' % (output_freqs.min() * 1e-6,
                                                                             output_freqs.max() * 1e-6,
                                                                             np.diff(output_freqs).mean() * 1e-6)

    hp_maps_interp = interpolate.interp1d(freqs, hp_maps, kind='cubic', axis=1)(output_freqs)

    print 'Creating SkyModel and saving it ...'

    # Create the SkyModel and save it
    init_params = dict()
    init_params['frequency'] = output_freqs
    init_params['name'] = 'HI-cube'
    init_params['spec_type'] = 'spectrum'
    init_params['spec_parms'] = dict()
    init_params['location'] = np.vstack([ra, dec]).T
    init_params['spectrum'] = hp_maps_interp
    init_params['src_shape'] = None

    sky_model = catalog.SkyModel(init_parms=init_params)
    sky_model.src_shape = None
    sky_model.save(os.path.join(args.out_dir, args.out_name))

    print 'All done !'


if __name__ == '__main__':
    main()
