# DATA directories
data_dir = './data/1ms/'

inp_ms_dir = data_dir + 'input_ms/'
lst_dir = data_dir + 'lst_cubes/'
lst_combined_dir = data_dir + 'lst_combined_cubes/'

data_col = 'CORRECTED_DATA'
use_enu = False
double_rotate = False

stokes_output = ['xx']

# Baseline config
do_red_bas_averaging = True
bmin = 10
bmax = 60

# LST bin config
do_lst_bin = True
bin_res_min = 2
longitude = 21.428

split_even_odd = False

# Outliers detection config
w_ratio = 0.2
sefd_ratio = 0.4

# GPR config
gpr_config_file = 'gpr_config_per.parset'
eor_bin_list = 'eor_bins.txt'
